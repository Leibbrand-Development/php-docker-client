# # ExecInspectResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**canRemove** | **bool** |  | [optional]
**detachKeys** | **string** |  | [optional]
**iD** | **string** |  | [optional]
**running** | **bool** |  | [optional]
**exitCode** | **int** |  | [optional]
**processConfig** | [**\ld\php\dockerClient\Model\ProcessConfig**](ProcessConfig.md) |  | [optional]
**openStdin** | **bool** |  | [optional]
**openStderr** | **bool** |  | [optional]
**openStdout** | **bool** |  | [optional]
**containerID** | **string** |  | [optional]
**pid** | **int** | The system process ID for the exec process. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
