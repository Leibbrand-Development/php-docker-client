# # VolumePruneResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**volumesDeleted** | **string[]** | Volumes that were deleted | [optional]
**spaceReclaimed** | **int** | Disk space reclaimed in bytes | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
