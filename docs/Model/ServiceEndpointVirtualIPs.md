# # ServiceEndpointVirtualIPs

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**networkID** | **string** |  | [optional]
**addr** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
