# # TaskStatusContainerStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**containerID** | **string** |  | [optional]
**pID** | **int** |  | [optional]
**exitCode** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
