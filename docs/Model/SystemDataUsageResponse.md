# # SystemDataUsageResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**layersSize** | **int** |  | [optional]
**images** | [**\ld\php\dockerClient\Model\ImageSummary[]**](ImageSummary.md) |  | [optional]
**containers** | [**\ld\php\dockerClient\Model\array[]**](array.md) |  | [optional]
**volumes** | [**\ld\php\dockerClient\Model\Volume[]**](Volume.md) |  | [optional]
**buildCache** | [**\ld\php\dockerClient\Model\BuildCache[]**](BuildCache.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
