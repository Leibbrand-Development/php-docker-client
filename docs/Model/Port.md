# # Port

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**iP** | **string** | Host IP address that the container&#39;s port is mapped to | [optional]
**privatePort** | **int** | Port on the container |
**publicPort** | **int** | Port exposed on the host | [optional]
**type** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
