# # PluginSettings

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mounts** | [**\ld\php\dockerClient\Model\PluginMount[]**](PluginMount.md) |  |
**env** | **string[]** |  |
**args** | **string[]** |  |
**devices** | [**\ld\php\dockerClient\Model\PluginDevice[]**](PluginDevice.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
