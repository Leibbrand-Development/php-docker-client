# # TaskSpecContainerSpecPrivileges

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credentialSpec** | [**\ld\php\dockerClient\Model\TaskSpecContainerSpecPrivilegesCredentialSpec**](TaskSpecContainerSpecPrivilegesCredentialSpec.md) |  | [optional]
**sELinuxContext** | [**\ld\php\dockerClient\Model\TaskSpecContainerSpecPrivilegesSELinuxContext**](TaskSpecContainerSpecPrivilegesSELinuxContext.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
