# # ServiceSpecMode

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**replicated** | [**\ld\php\dockerClient\Model\ServiceSpecModeReplicated**](ServiceSpecModeReplicated.md) |  | [optional]
**global** | **object** |  | [optional]
**replicatedJob** | [**\ld\php\dockerClient\Model\ServiceSpecModeReplicatedJob**](ServiceSpecModeReplicatedJob.md) |  | [optional]
**globalJob** | **object** | The mode used for services which run a task to the completed state on each valid node. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
