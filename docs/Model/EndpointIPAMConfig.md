# # EndpointIPAMConfig

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**iPv4Address** | **string** |  | [optional]
**iPv6Address** | **string** |  | [optional]
**linkLocalIPs** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
