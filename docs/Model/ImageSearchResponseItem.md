# # ImageSearchResponseItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **string** |  | [optional]
**isOfficial** | **bool** |  | [optional]
**isAutomated** | **bool** |  | [optional]
**name** | **string** |  | [optional]
**starCount** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
