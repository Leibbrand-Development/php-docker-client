# # ServiceJobStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**jobIteration** | [**\ld\php\dockerClient\Model\ObjectVersion**](ObjectVersion.md) |  | [optional]
**lastExecution** | **string** | The last time, as observed by the server, that this job was started. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
