# # NodeDescription

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hostname** | **string** |  | [optional]
**platform** | [**\ld\php\dockerClient\Model\Platform**](Platform.md) |  | [optional]
**resources** | [**\ld\php\dockerClient\Model\ResourceObject**](ResourceObject.md) |  | [optional]
**engine** | [**\ld\php\dockerClient\Model\EngineDescription**](EngineDescription.md) |  | [optional]
**tLSInfo** | [**\ld\php\dockerClient\Model\TLSInfo**](TLSInfo.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
