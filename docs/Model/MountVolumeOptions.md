# # MountVolumeOptions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**noCopy** | **bool** | Populate volume with data from the target. | [optional] [default to false]
**labels** | **array<string,string>** | User-defined key/value metadata. | [optional]
**driverConfig** | [**\ld\php\dockerClient\Model\MountVolumeOptionsDriverConfig**](MountVolumeOptionsDriverConfig.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
