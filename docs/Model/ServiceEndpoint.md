# # ServiceEndpoint

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**spec** | [**\ld\php\dockerClient\Model\EndpointSpec**](EndpointSpec.md) |  | [optional]
**ports** | [**\ld\php\dockerClient\Model\EndpointPortConfig[]**](EndpointPortConfig.md) |  | [optional]
**virtualIPs** | [**\ld\php\dockerClient\Model\ServiceEndpointVirtualIPs[]**](ServiceEndpointVirtualIPs.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
