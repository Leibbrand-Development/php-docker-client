# # ServiceSpec

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Name of the service. | [optional]
**labels** | **array<string,string>** | User-defined key/value metadata. | [optional]
**taskTemplate** | [**\ld\php\dockerClient\Model\TaskSpec**](TaskSpec.md) |  | [optional]
**mode** | [**\ld\php\dockerClient\Model\ServiceSpecMode**](ServiceSpecMode.md) |  | [optional]
**updateConfig** | [**\ld\php\dockerClient\Model\ServiceSpecUpdateConfig**](ServiceSpecUpdateConfig.md) |  | [optional]
**rollbackConfig** | [**\ld\php\dockerClient\Model\ServiceSpecRollbackConfig**](ServiceSpecRollbackConfig.md) |  | [optional]
**networks** | [**\ld\php\dockerClient\Model\NetworkAttachmentConfig[]**](NetworkAttachmentConfig.md) | Specifies which networks the service should attach to. | [optional]
**endpointSpec** | [**\ld\php\dockerClient\Model\EndpointSpec**](EndpointSpec.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
