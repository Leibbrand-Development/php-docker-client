# # DistributionInspectResponsePlatforms

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**architecture** | **string** |  | [optional]
**oS** | **string** |  | [optional]
**oSVersion** | **string** |  | [optional]
**oSFeatures** | **string[]** |  | [optional]
**variant** | **string** |  | [optional]
**features** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
