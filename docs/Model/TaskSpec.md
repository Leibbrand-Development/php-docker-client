# # TaskSpec

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pluginSpec** | [**\ld\php\dockerClient\Model\TaskSpecPluginSpec**](TaskSpecPluginSpec.md) |  | [optional]
**containerSpec** | [**\ld\php\dockerClient\Model\TaskSpecContainerSpec**](TaskSpecContainerSpec.md) |  | [optional]
**networkAttachmentSpec** | [**\ld\php\dockerClient\Model\TaskSpecNetworkAttachmentSpec**](TaskSpecNetworkAttachmentSpec.md) |  | [optional]
**resources** | [**\ld\php\dockerClient\Model\TaskSpecResources**](TaskSpecResources.md) |  | [optional]
**restartPolicy** | [**\ld\php\dockerClient\Model\TaskSpecRestartPolicy**](TaskSpecRestartPolicy.md) |  | [optional]
**placement** | [**\ld\php\dockerClient\Model\TaskSpecPlacement**](TaskSpecPlacement.md) |  | [optional]
**forceUpdate** | **int** | A counter that triggers an update even if no relevant parameters have been changed. | [optional]
**runtime** | **string** | Runtime is the type of runtime specified for the task executor. | [optional]
**networks** | [**\ld\php\dockerClient\Model\NetworkAttachmentConfig[]**](NetworkAttachmentConfig.md) | Specifies which networks the service should attach to. | [optional]
**logDriver** | [**\ld\php\dockerClient\Model\TaskSpecLogDriver**](TaskSpecLogDriver.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
