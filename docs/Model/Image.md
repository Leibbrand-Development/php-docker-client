# # Image

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  |
**repoTags** | **string[]** |  | [optional]
**repoDigests** | **string[]** |  | [optional]
**parent** | **string** |  |
**comment** | **string** |  |
**created** | **string** |  |
**container** | **string** |  |
**containerConfig** | [**\ld\php\dockerClient\Model\ContainerConfig**](ContainerConfig.md) |  | [optional]
**dockerVersion** | **string** |  |
**author** | **string** |  |
**config** | [**\ld\php\dockerClient\Model\ContainerConfig**](ContainerConfig.md) |  | [optional]
**architecture** | **string** |  |
**os** | **string** |  |
**osVersion** | **string** |  | [optional]
**size** | **int** |  |
**virtualSize** | **int** |  |
**graphDriver** | [**\ld\php\dockerClient\Model\GraphDriverData**](GraphDriverData.md) |  |
**rootFS** | [**\ld\php\dockerClient\Model\ImageRootFS**](ImageRootFS.md) |  |
**metadata** | [**\ld\php\dockerClient\Model\ImageMetadata**](ImageMetadata.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
