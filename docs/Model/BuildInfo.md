# # BuildInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional]
**stream** | **string** |  | [optional]
**error** | **string** |  | [optional]
**errorDetail** | [**\ld\php\dockerClient\Model\ErrorDetail**](ErrorDetail.md) |  | [optional]
**status** | **string** |  | [optional]
**progress** | **string** |  | [optional]
**progressDetail** | [**\ld\php\dockerClient\Model\ProgressDetail**](ProgressDetail.md) |  | [optional]
**aux** | [**\ld\php\dockerClient\Model\ImageID**](ImageID.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
