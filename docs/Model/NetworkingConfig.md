# # NetworkingConfig

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**endpointsConfig** | [**array<string,\ld\php\dockerClient\Model\EndpointSettings>**](EndpointSettings.md) | A mapping of network name to endpoint configuration for that network. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
