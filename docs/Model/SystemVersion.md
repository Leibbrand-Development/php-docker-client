# # SystemVersion

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**platform** | [**\ld\php\dockerClient\Model\SystemVersionPlatform**](SystemVersionPlatform.md) |  | [optional]
**components** | [**\ld\php\dockerClient\Model\SystemVersionComponents[]**](SystemVersionComponents.md) | Information about system components | [optional]
**version** | **string** | The version of the daemon | [optional]
**apiVersion** | **string** | The default (and highest) API version that is supported by the daemon | [optional]
**minAPIVersion** | **string** | The minimum API version that is supported by the daemon | [optional]
**gitCommit** | **string** | The Git commit of the source code that was used to build the daemon | [optional]
**goVersion** | **string** | The version Go used to compile the daemon, and the version of the Go runtime in use. | [optional]
**os** | **string** | The operating system that the daemon is running on (\&quot;linux\&quot; or \&quot;windows\&quot;) | [optional]
**arch** | **string** | The architecture that the daemon is running on | [optional]
**kernelVersion** | **string** | The kernel version (&#x60;uname -r&#x60;) that the daemon is running on.  This field is omitted when empty. | [optional]
**experimental** | **bool** | Indicates if the daemon is started with experimental features enabled.  This field is omitted when empty / false. | [optional]
**buildTime** | **string** | The date and time that the daemon was compiled. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
