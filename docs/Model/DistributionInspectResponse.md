# # DistributionInspectResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**descriptor** | [**\ld\php\dockerClient\Model\DistributionInspectResponseDescriptor**](DistributionInspectResponseDescriptor.md) |  |
**platforms** | [**\ld\php\dockerClient\Model\DistributionInspectResponsePlatforms[]**](DistributionInspectResponsePlatforms.md) | An array containing all platforms supported by the image. |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
