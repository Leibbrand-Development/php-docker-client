# # TaskSpecContainerSpecSecrets

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | [**\ld\php\dockerClient\Model\TaskSpecContainerSpecFile**](TaskSpecContainerSpecFile.md) |  | [optional]
**secretID** | **string** | SecretID represents the ID of the specific secret that we&#39;re referencing. | [optional]
**secretName** | **string** | SecretName is the name of the secret that this references, but this is just provided for lookup/display purposes. The secret in the reference will be identified by its ID. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
