# # PluginConfig

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dockerVersion** | **string** | Docker Version used to create the plugin | [optional]
**description** | **string** |  |
**documentation** | **string** |  |
**interface** | [**\ld\php\dockerClient\Model\PluginConfigInterface**](PluginConfigInterface.md) |  |
**entrypoint** | **string[]** |  |
**workDir** | **string** |  |
**user** | [**\ld\php\dockerClient\Model\PluginConfigUser**](PluginConfigUser.md) |  | [optional]
**network** | [**\ld\php\dockerClient\Model\PluginConfigNetwork**](PluginConfigNetwork.md) |  |
**linux** | [**\ld\php\dockerClient\Model\PluginConfigLinux**](PluginConfigLinux.md) |  |
**propagatedMount** | **string** |  |
**ipcHost** | **bool** |  |
**pidHost** | **bool** |  |
**mounts** | [**\ld\php\dockerClient\Model\PluginMount[]**](PluginMount.md) |  |
**env** | [**\ld\php\dockerClient\Model\PluginEnv[]**](PluginEnv.md) |  |
**args** | [**\ld\php\dockerClient\Model\PluginConfigArgs**](PluginConfigArgs.md) |  |
**rootfs** | [**\ld\php\dockerClient\Model\PluginConfigRootfs**](PluginConfigRootfs.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
