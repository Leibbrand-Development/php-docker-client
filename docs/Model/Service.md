# # Service

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**iD** | **string** |  | [optional]
**version** | [**\ld\php\dockerClient\Model\ObjectVersion**](ObjectVersion.md) |  | [optional]
**createdAt** | **string** |  | [optional]
**updatedAt** | **string** |  | [optional]
**spec** | [**\ld\php\dockerClient\Model\ServiceSpec**](ServiceSpec.md) |  | [optional]
**endpoint** | [**\ld\php\dockerClient\Model\ServiceEndpoint**](ServiceEndpoint.md) |  | [optional]
**updateStatus** | [**\ld\php\dockerClient\Model\ServiceUpdateStatus**](ServiceUpdateStatus.md) |  | [optional]
**serviceStatus** | [**\ld\php\dockerClient\Model\ServiceServiceStatus**](ServiceServiceStatus.md) |  | [optional]
**jobStatus** | [**\ld\php\dockerClient\Model\ServiceJobStatus**](ServiceJobStatus.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
