# # Config

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**iD** | **string** |  | [optional]
**version** | [**\ld\php\dockerClient\Model\ObjectVersion**](ObjectVersion.md) |  | [optional]
**createdAt** | **string** |  | [optional]
**updatedAt** | **string** |  | [optional]
**spec** | [**\ld\php\dockerClient\Model\ConfigSpec**](ConfigSpec.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
