# # SwarmSpec

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Name of the swarm. | [optional]
**labels** | **array<string,string>** | User-defined key/value metadata. | [optional]
**orchestration** | [**\ld\php\dockerClient\Model\SwarmSpecOrchestration**](SwarmSpecOrchestration.md) |  | [optional]
**raft** | [**\ld\php\dockerClient\Model\SwarmSpecRaft**](SwarmSpecRaft.md) |  | [optional]
**dispatcher** | [**\ld\php\dockerClient\Model\SwarmSpecDispatcher**](SwarmSpecDispatcher.md) |  | [optional]
**cAConfig** | [**\ld\php\dockerClient\Model\SwarmSpecCAConfig**](SwarmSpecCAConfig.md) |  | [optional]
**encryptionConfig** | [**\ld\php\dockerClient\Model\SwarmSpecEncryptionConfig**](SwarmSpecEncryptionConfig.md) |  | [optional]
**taskDefaults** | [**\ld\php\dockerClient\Model\SwarmSpecTaskDefaults**](SwarmSpecTaskDefaults.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
