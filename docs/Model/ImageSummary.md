# # ImageSummary

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  |
**parentId** | **string** |  |
**repoTags** | **string[]** |  |
**repoDigests** | **string[]** |  |
**created** | **int** |  |
**size** | **int** |  |
**sharedSize** | **int** |  |
**virtualSize** | **int** |  |
**labels** | **array<string,string>** |  |
**containers** | **int** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
