# # DistributionInspectResponseDescriptor

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mediaType** | **string** |  | [optional]
**size** | **int** |  | [optional]
**digest** | **string** |  | [optional]
**uRLs** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
