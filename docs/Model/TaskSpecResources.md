# # TaskSpecResources

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**limits** | [**\ld\php\dockerClient\Model\Limit**](Limit.md) |  | [optional]
**reservations** | [**\ld\php\dockerClient\Model\ResourceObject**](ResourceObject.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
