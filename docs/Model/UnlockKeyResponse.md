# # UnlockKeyResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**unlockKey** | **string** | The swarm&#39;s unlock key. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
