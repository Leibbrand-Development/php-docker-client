# # BuildCache

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**iD** | **string** |  | [optional]
**parent** | **string** |  | [optional]
**type** | **string** |  | [optional]
**description** | **string** |  | [optional]
**inUse** | **bool** |  | [optional]
**shared** | **bool** |  | [optional]
**size** | **int** | Amount of disk space used by the build cache (in bytes). | [optional]
**createdAt** | **string** | Date and time at which the build cache was created in [RFC 3339](https://www.ietf.org/rfc/rfc3339.txt) format with nano-seconds. | [optional]
**lastUsedAt** | **string** | Date and time at which the build cache was last used in [RFC 3339](https://www.ietf.org/rfc/rfc3339.txt) format with nano-seconds. | [optional]
**usageCount** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
