# # Node

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**iD** | **string** |  | [optional]
**version** | [**\ld\php\dockerClient\Model\ObjectVersion**](ObjectVersion.md) |  | [optional]
**createdAt** | **string** | Date and time at which the node was added to the swarm in [RFC 3339](https://www.ietf.org/rfc/rfc3339.txt) format with nano-seconds. | [optional]
**updatedAt** | **string** | Date and time at which the node was last updated in [RFC 3339](https://www.ietf.org/rfc/rfc3339.txt) format with nano-seconds. | [optional]
**spec** | [**\ld\php\dockerClient\Model\NodeSpec**](NodeSpec.md) |  | [optional]
**description** | [**\ld\php\dockerClient\Model\NodeDescription**](NodeDescription.md) |  | [optional]
**status** | [**\ld\php\dockerClient\Model\NodeStatus**](NodeStatus.md) |  | [optional]
**managerStatus** | [**\ld\php\dockerClient\Model\ManagerStatus**](ManagerStatus.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
