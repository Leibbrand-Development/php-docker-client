# # TaskStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **string** |  | [optional]
**state** | [**\ld\php\dockerClient\Model\TaskState**](TaskState.md) |  | [optional]
**message** | **string** |  | [optional]
**err** | **string** |  | [optional]
**containerStatus** | [**\ld\php\dockerClient\Model\TaskStatusContainerStatus**](TaskStatusContainerStatus.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
