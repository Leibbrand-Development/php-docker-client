# # SwarmSpecDispatcher

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**heartbeatPeriod** | **int** | The delay for an agent to send a heartbeat to the dispatcher. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
