# # PluginConfigLinux

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**capabilities** | **string[]** |  |
**allowAllDevices** | **bool** |  |
**devices** | [**\ld\php\dockerClient\Model\PluginDevice[]**](PluginDevice.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
