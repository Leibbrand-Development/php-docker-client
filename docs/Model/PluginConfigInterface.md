# # PluginConfigInterface

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**types** | [**\ld\php\dockerClient\Model\PluginInterfaceType[]**](PluginInterfaceType.md) |  |
**socket** | **string** |  |
**protocolScheme** | **string** | Protocol to use for clients connecting to the plugin. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
