# # NetworkPruneResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**networksDeleted** | **string[]** | Networks that were deleted | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
