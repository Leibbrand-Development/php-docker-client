# # ServiceSpecModeReplicatedJob

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maxConcurrent** | **int** | The maximum number of replicas to run simultaneously. | [optional] [default to 1]
**totalCompletions** | **int** | The total number of replicas desired to reach the Completed state. If unset, will default to the value of &#x60;MaxConcurrent&#x60; | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
