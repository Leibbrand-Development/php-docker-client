# # NetworkContainer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional]
**endpointID** | **string** |  | [optional]
**macAddress** | **string** |  | [optional]
**iPv4Address** | **string** |  | [optional]
**iPv6Address** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
