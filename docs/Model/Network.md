# # Network

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional]
**id** | **string** |  | [optional]
**created** | **string** |  | [optional]
**scope** | **string** |  | [optional]
**driver** | **string** |  | [optional]
**enableIPv6** | **bool** |  | [optional]
**iPAM** | [**\ld\php\dockerClient\Model\IPAM**](IPAM.md) |  | [optional]
**internal** | **bool** |  | [optional]
**attachable** | **bool** |  | [optional]
**ingress** | **bool** |  | [optional]
**containers** | [**array<string,\ld\php\dockerClient\Model\NetworkContainer>**](NetworkContainer.md) |  | [optional]
**options** | **array<string,string>** |  | [optional]
**labels** | **array<string,string>** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
