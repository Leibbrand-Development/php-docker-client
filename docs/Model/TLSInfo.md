# # TLSInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**trustRoot** | **string** | The root CA certificate(s) that are used to validate leaf TLS certificates. | [optional]
**certIssuerSubject** | **string** | The base64-url-safe-encoded raw subject bytes of the issuer. | [optional]
**certIssuerPublicKey** | **string** | The base64-url-safe-encoded raw public key bytes of the issuer. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
