# # SystemEventsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | The type of object emitting the event | [optional]
**action** | **string** | The type of event | [optional]
**actor** | [**\ld\php\dockerClient\Model\SystemEventsResponseActor**](SystemEventsResponseActor.md) |  | [optional]
**time** | **int** | Timestamp of event | [optional]
**timeNano** | **int** | Timestamp of event, with nanosecond accuracy | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
