# # Limit

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nanoCPUs** | **int** |  | [optional]
**memoryBytes** | **int** |  | [optional]
**pids** | **int** | Limits the maximum number of PIDs in the container. Set &#x60;0&#x60; for unlimited. | [optional] [default to 0]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
