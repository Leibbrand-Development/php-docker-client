# # EngineDescription

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**engineVersion** | **string** |  | [optional]
**labels** | **array<string,string>** |  | [optional]
**plugins** | [**\ld\php\dockerClient\Model\EngineDescriptionPlugins[]**](EngineDescriptionPlugins.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
