# # ContainerWaitResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**statusCode** | **int** | Exit code of the container |
**error** | [**\ld\php\dockerClient\Model\ContainerWaitResponseError**](ContainerWaitResponseError.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
