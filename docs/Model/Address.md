# # Address

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**addr** | **string** | IP address. | [optional]
**prefixLen** | **int** | Mask length of the IP address. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
